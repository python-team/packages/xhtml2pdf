<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
"http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd">

<refentry>
  <refmeta>
    <refentrytitle>xhtml2pdf3</refentrytitle>
    <manvolnum>1</manvolnum>
    <refmiscinfo class="source">pisa</refmiscinfo>
    <refmiscinfo class="manual">User Commands</refmiscinfo>
  </refmeta>
  <refnamediv>
    <refname>xhtml2pdf3</refname>

    <refpurpose>PDF generator using HTML and CSS</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>xhtml2pdf3</command>

      <arg><option>-b <replaceable>base path</replaceable></option></arg>

      <arg><option>--base=<replaceable>base path</replaceable></option></arg>

      <arg><option>-c <replaceable>CSS file</replaceable></option></arg>

      <arg><option>--css=<replaceable>CSS file</replaceable></option></arg>

      <arg><option>--css-dump</option></arg>

      <arg><option>-d</option></arg>

      <arg><option>--debug</option></arg>

      <arg><option>--encoding=<replaceable>character encoding</replaceable></option></arg>

      <arg><option>-h</option></arg>

      <arg><option>--help</option></arg>

      <arg><option>-q</option></arg>

      <arg><option>--quiet</option></arg>

      <arg><option>--version</option></arg>

      <arg><option>-w</option></arg>

      <arg><option>--warn</option></arg>

      <arg><option>-x</option></arg>

      <arg><option>--xml</option></arg>

      <arg><option>--xhtml</option></arg>

      <arg><option>--html</option></arg>

      <arg>SRC</arg>

      <arg>DEST</arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>Description</title>
    <para>
      This manual page documents briefly the xhtml2pdf3 command.
    </para>
    <para>
       xhtml2pdf3 is an HTML-to-PDF converter using the ReportLab
       Toolkit, HTML5lib and pyPdf.
    </para>
    <para>
       It supports HTML 5 and CSS 2.1 (and some of CSS 3).  It is
       completely written in pure Python so it is platform
       independent. The main benefit of this tool that a user with Web
       skills like HTML and CSS is able to generate PDF templates very
       quickly without learning new technologies.  Easy integration
       into Python frameworks like CherryPy, KID Templating,
       TurboGears, Django, Zope, Plone, Google AppEngine (GAE) etc.
    </para>
  </refsect1>
  <refsect1>
    <title>Options</title>
    <para>
      A summary of options is included below.
    </para>
    <variablelist>
      <varlistentry>
	<term>SRC</term>
	<listitem>
	  <para>
	    Name of a HTML file or a file pattern using
	    <literal>*</literal> placeholder.  If you want to read
	    from stdin use <literal>-</literal> as file name.  You may
	    also load an URL over HTTP. Take care of putting the
	    <emphasis>src</emphasis> in quotes if it contains
	    characters like <literal>?</literal>.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term>DEST</term>
	<listitem>
	  <para>
	    Name of the generated PDF file or <literal>-</literal> if
	    you like to send the result to stdout. Take care that the
	    destination file is not already opened by any other
	    application like the Adobe Reader. If the destination is
	    not writeable a similar name will be calculated
	    automatically.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-b</option></term>
	<term><option>--base</option></term>
	<listitem>
	  <para>
	    Specify a base path if input comes via STDIN.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-c</option></term>
	<term><option>--css</option></term>
	<listitem>
	  <para>
	    Path to default CSS file
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>--css-dump</option></term>
	<listitem>
	  <para>
	    Dumps the default CSS definitions to STDOUT.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-d</option></term>
	<term><option>--debug</option></term>
	<listitem>
	  <para>
	    Show debugging information.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>--encoding</option></term>
	<listitem>
	  <para>
	    The character encoding of SRC. If left empty (default)
	    this information will be extracted from the HTML header
	    data.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-h</option></term>
	<term><option>--help</option></term>
	<listitem>
	  <para>
	    Show the help text.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-q</option></term>
	<term><option>--quiet</option></term>
	<listitem>
	  <para>
	    Show no messages.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>--version</option></term>
	<listitem>
	  <para>
	    Show version information.
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-w</option></term>
	<term><option>--warn</option></term>
	<listitem>
	  <para>
	    Show warnings
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-x</option></term>
	<term><option>--xml</option></term>
	<term><option>--xhtml</option></term>
	<listitem>
	  <para>
	    Force parsing in XML mode (automatically used if SRC ends
	    with <filename>.xml</filename>).
	  </para>
	</listitem>
      </varlistentry>
      <varlistentry>
	<term><option>--html</option></term>
	<listitem>
	  <para>
	    Force parsin in HTML mode (default).
	  </para>
	</listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
  <refsect1>
    <title>Author</title>
    <para>
      xhtml2pdf3 was written by Dirk Holtwick
      <email>&lt;dirk.holtwick@gmail.com&gt;</email>.
    </para>
    <para>
       This manual page was written by Toby Smithe
       <email>&lt;tsmithe@ubuntu.com&gt;</email>, for the Debian
       project (but may be used by others).
     </para>
  </refsect1>
</refentry>
