xhtml2pdf (0.2.17+dfsg-1) unstable; urgency=medium

  * New upstream version 0.2.17.
  * Revert "Update import for breaking changes in python-bidi 0.5.0".

 -- Bastian Germann <bage@debian.org>  Mon, 24 Feb 2025 08:18:58 +0100

xhtml2pdf (0.2.16+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * CVE-2024-25885: Fix reDOS CVE in getColor function (closes: #1084986).

 -- Colin Watson <cjwatson@debian.org>  Mon, 03 Feb 2025 16:39:02 +0000

xhtml2pdf (0.2.15+dfsg-2) unstable; urgency=medium

  * Team upload
  * Prevent unnecessary man rename
  * Fix lintian
  * Run tests with pytest and exclude remote ones; Closes: #1069809

 -- Bastian Germann <bage@debian.org>  Wed, 15 May 2024 17:46:35 +0200

xhtml2pdf (0.2.15+dfsg-1) unstable; urgency=medium

  * Team upload.

  * Exclude font files from source tree
  * d/copyright: Add BSD-3-clause~TechGame license
  * Drop nose in favour of unittest; Closes: #1018667
  * Patch: Make pyHanko optional
  * Patch: Adapt to new ShowBoundaryValue import

  [ Georges Khaznadar ]
  * adjusted the configuration for gbp (file debian/gbp.conf)
  * New upstream version 0.2.15; Closes: #610390; Closes: #592500
  * updated debian patches
  * updated d/python3-xhtml2pdf.docs
  * new build-dependencies: pybuild-plugin-pyproject, python3-asn1crypto,
    python3-pyhanko-certvalidator, python3-svglib
  * replaced a build-dependency: python3-pypdf2 -> python3-pypdf.
    Closes: #1029739

 -- Bastian Germann <bage@debian.org>  Mon, 26 Feb 2024 21:01:52 +0000

xhtml2pdf (0.2.5-5) unstable; urgency=medium

  * Team upload.
  * defined a minimum version for the build-dependency on python3-reportlab
  * bumped Standards-Version: 4.6.2
  * added a patch: d/patches/fix-invalide-escape-sequences.patch, because
    py3compile raised a few SyntaxWarning, about incorrect escape sequences

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 17 Feb 2024 13:24:35 +0100

xhtml2pdf (0.2.5-4) unstable; urgency=medium

  * Team upload.
  * added d/source/lintian-overrides, with comments explaining the contents
    of the not-so-sourceless HTML files reported by lintian.
  * kept the modifications initiated in release 0.2.5-3.1; Closes: #1064037

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 16 Feb 2024 15:36:27 +0100

xhtml2pdf (0.2.5-3.1) unstable; urgency=medium

  * NMU: patched xhtml2pdf/context.py, because ShowBoundaryValue is
    provided by reportlab.pdfgen.canvas, since the last version of
    python3-reportlab
  * added missing build-dependencies on python3-bidi, python3-arabic-reshaper

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 16 Feb 2024 09:48:10 +0100

xhtml2pdf (0.2.5-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends-Indep: Drop versioned constraint on python3-html5lib,
      python3-pypdf2 and python3-reportlab.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 30 Jun 2022 19:43:01 +0100

xhtml2pdf (0.2.5-2) unstable; urgency=medium

  * Add R³=no.

 -- Martin <debacle@debian.org>  Mon, 25 Apr 2022 21:28:43 +0000

xhtml2pdf (0.2.5-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

  [ Drew Parsons ]
  * update debian/watch to latest recommended github format (using
    tags). Otherwise v0.2.5 is missed.
  * New upstream release.
  * debian patch missing_bidi.patch skips bidi handling since bidi and
    arabic_reshaper are not yet packaged. Packaging has been requested
    in RFP#921336 and RFP#1009131.
  * debian patch new_reportlab_API_PR590.patch adapts upstream PR#590
    to update code for latest reportlab API.
    Closes: #1007012, #1008336.
  * Build-Depends-Indep: python3-pil, python3-reportlab (>= 3.3.0~)

 -- Drew Parsons <dparsons@debian.org>  Thu, 07 Apr 2022 18:06:02 +0200

xhtml2pdf (0.2.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - deprecates debian patch use_html_not_cgi.patch,
      instead Depends: python3-six
  * Standards-Version: 4.5.0

 -- Drew Parsons <dparsons@debian.org>  Sat, 25 Jan 2020 11:05:29 +0800

xhtml2pdf (0.2.2-5) unstable; urgency=medium

  * Team upload.

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation
    lines.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Drew Parsons ]
  * debian/patches/use_html_not_cgi.patch replaces deprecated
    cgi.escape with html.escape. Closes: #948788.

 -- Drew Parsons <dparsons@debian.org>  Fri, 17 Jan 2020 21:36:40 +0800

xhtml2pdf (0.2.2-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Sandro Tosi ]
  * Drop python2 support; Closes: #938845, #935191

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jan 2020 13:33:34 -0500

xhtml2pdf (0.2.2-3) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Andrey Rahmatullin ]
  * Drop the obsolete B-D on python-turbogears2.

 -- Andrey Rahmatullin <wrar@debian.org>  Fri, 09 Aug 2019 14:32:42 +0500

xhtml2pdf (0.2.2-2) unstable; urgency=medium

  * Team upload.
  * tests require python-nose

 -- Drew Parsons <dparsons@debian.org>  Sat, 05 Jan 2019 17:11:45 +0100

xhtml2pdf (0.2.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

  [ Ondřej Nový ]
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout

  [ Drew Parsons ]
  * Standards-Version: 4.3.0
  * debhelper compatibility level 12
  * place docs, examples and upstream changelog in python3-xhtml2pdf
  * Build-Depends: python-html5lib (>= 1.0)
    (drop patch Allow-html5lib-0.999999999.patch)
  * add debian/tests (autopkgtest)

 -- Drew Parsons <dparsons@debian.org>  Sat, 05 Jan 2019 12:29:11 +0100

xhtml2pdf (0.2.1-1) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Drew Parsons ]
  * New upstream version.
    - drops turbogears support
      (remove debian patch Ignore-turbogears.patch)
  * Standards-Version: 4.1.3
  * debhelper compatibility level 11

 -- Drew Parsons <dparsons@debian.org>  Thu, 22 Feb 2018 17:32:09 +0800

xhtml2pdf (0.2b-2) unstable; urgency=medium

  * Team upload.
  * python-xhtml2pdf Conflicts: and Replaces: python-pisa.
    Closes: #884513.
  * Standards-Version: 4.1.2

 -- Drew Parsons <dparsons@debian.org>  Sat, 16 Dec 2017 17:47:02 +0800

xhtml2pdf (0.2b-1) unstable; urgency=medium

  [ Hugo Lefeuvre ]
  * Team upload.
  * New upstream release.
  * Refresh patches for new upstream release.

  [ Drew Parsons ]
  * Create new debian packaging for xhtml2pdf, adapted from pisa.
    - includes python3 package, python3-xhtml2pdf
    - use ${python:Depends} to pull in python dependencies
    - Closes: #879839.
  * Standards-Version: 4.1.1
  * debhelper compatibility level 10

 -- Drew Parsons <dparsons@debian.org>  Mon, 30 Oct 2017 12:38:03 +0800

pisa (3.0.32-4) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)

  [ Luciano Bello ]
  * Removing dependency to python-pypdf in favor of python-pypdf2.
    (Closes: #763981)
  * Standards-Version updated.

 -- Luciano Bello <luciano@debian.org>  Thu, 09 Jun 2016 15:27:11 +0200

pisa (3.0.32-3) unstable; urgency=low

  * Team upload.

  [ Piotr Ożarowski ]
  * Bump minimum required python-reportlab version to 2.2
    (see sx/pisa3/__init__.py)

  [ Matthias Schmitz ]
  * Switch to debhelper 9
  * Switch to source format 3.0 (quilt), rework patches from dpatch to quilt
  * Bump Standards version to 3.9.5
  * Add patch to fix wrong version detection of reportlab.
    (Closes: #571120)
  * To please lintian: re-word the manpage,
    add a patch to fix wrong interpreter path in examples,
    remove deprecated XB-Python-Version:,
    substitute python-dev by python in Build-Depends
  * Add python-pkg-resources to Depends: (Closes: #587221)

  [ SVN-Git Migration ]
  * git-dpm config.
  * Update Vcs fields for git migration.

  [ Mattia Rizzolo ]
  * Remove long useless debian/pycompat.
  * debian/watch: rewrite using pypi.debian.net redirector.
  * debian/control:
    + switch dependecy on python-support to dh-python.  Closes: #786222
    + Bump standards-versions to 3.9.6, no changes needed.

 -- Mattia Rizzolo <mattia@debian.org>  Mon, 14 Dec 2015 00:57:40 +0000

pisa (3.0.32-2) unstable; urgency=medium

  * Team upload.
  * Add debian/patches/02-reportlab_bigger_than_two.patch.dpatch to fix
    reportlab version compatibility problems (Closes: #571120)
    - Thanks to Walter Doekes and Jamie McClelland for patches - both were
      fine patches, but the one used was the smaller change since Debian is in
      pre-release freeze

 -- Scott Kitterman <scott@kitterman.com>  Sun, 23 Nov 2014 21:51:06 -0500

pisa (3.0.32-1) unstable; urgency=low

  * Initial release (Closes: #504277). Thanks to Toby Smithe
    <tsmithe@ubuntu.com> for preparing the package. Thanks to Thomas
    Bechtold for helping with the licenses. And, yes, the file
    demo/tgpisa/tgpisa/release.py is just an example, not necessarily
    licensed under MIT license. CODE2000.TTF removed from source,
    package is under GPL-2.

 -- W. Martin Borgert <debacle@debian.org>  Mon, 08 Feb 2010 22:02:40 +0000
